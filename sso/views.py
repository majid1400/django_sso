import os
import json
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.http import HttpResponse
from django.shortcuts import render, redirect
import requests
from django.http import Http404
from django.conf import settings
from django.contrib.auth import authenticate, login

from ssoProject.local_settings import data_oxd

User = get_user_model

def post_data(end_point, data, access_token):
    """Posts data to oxd server"""
    headers = {
        'Content-type': 'application/json',
        'Authorization': "Bearer " + access_token
    }
    result = requests.post(
        data_oxd['oxd_server'] + end_point,
        data=json.dumps(data),
        headers=headers,
        verify=False
    )
    return result.json()

def registration(request):
    data = {
        "op_host": data_oxd['op_host'],
        "scope": ["openid", "oxd", "profile", "email"],
        "client_id": data_oxd['client_id'],
        "client_secret": data_oxd['client_secret']
    }
    
    try:
        result = post_data('get-client-token', data, '')
        access_token = result['data']['access_token']
    except:
        raise Http404("خطای سرویس، لطفا با پشتیبان تماس بگیرید.")
    
    # Since cgi scripts runs per request, we need access_token in subsequent queries.
    # Thus write a file. access_token should be secured and better to use session
    # in web frameworks.
    with open(os.path.join(settings.BASE_DIR, 'sso/access_token.txt'), 'w') as f:
        f.write(access_token)
    
    data = {
        "oxd_id": data_oxd['oxd_id'],
        "scope": ["openid", "profile", "email"],
    }
    
    # [3] User will be directed to Gluu Server to login, so we need an url for login
    result = post_data('get-authorization-url', data, access_token)
    
    return HttpResponse('<a href="{0}">Click here to login</a>'.format(result['data']['authorization_url']))

def callback(request):
    code = request.GET.get('code')
    state = request.GET.get('state')
    if code:
        access_token = open(os.path.join(settings.BASE_DIR,'sso/access_token.txt')).read()
        
        
        data = {
            "oxd_id": data_oxd['oxd_id'],
            "code": code,
            "state": state
        }
        result = post_data('get-tokens-by-code', data, access_token)
        if result['status'] == "error":
            return redirect("/api/sso/login/")
        
        data = {
            "oxd_id": data_oxd['oxd_id'],
            "access_token": result['data']['access_token']
        }
        result = post_data('get-user-info', data, access_token)
        userName = result['data']['claims']['name'][0]
        
        
        # return HttpResponse('<br><b>name :</b> {} <br><a href="logoutme">Click here to logout</a>'.format(result['data']['claims']['name'][0]))
        # return render(request, 'user/profile.html', context)
        return redirect(home, userName)
    return redirect("/api/sso/registration/")

def home(request, userName):
    context = {"name":userName,'logout':'<a href="logoutme">Click here to logout</a>'}
    return render(request, 'user/profile.html', context)


def logout(request):
    data = {
        "oxd_id": data_oxd['oxd_id'],
        "post_logout_redirect_uri": "http://localhost:8000/api/sso/login/"
    }

    # Read access_token that we previously saved
    oxd_access_token = open(os.path.join(settings.BASE_DIR,'sso/access_token.txt')).read()

    # [6] Request logout uri from oxd server.
    result = post_data('get-logout-uri', data, oxd_access_token)

    return redirect(result['data']['uri'])


def testViews(request):
    return HttpResponse("<h1>In these is page test.</h1>")