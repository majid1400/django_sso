from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractUser, UserManager, PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

class User(AbstractBaseUser, PermissionsMixin):
    password = models.CharField(_('password'), max_length=128, blank=True)
    username = models.CharField(_('username'),max_length=150,unique=True)
    groupUser = models.TextField(_('group user sso'),blank=True)
    is_delete = models.BooleanField(_("is delete"), default=False)
    
    is_active = models.BooleanField(_('active'),default=True,)
    is_staff = models.BooleanField(_('staff status'),default=False)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    objects = UserManager()
    USERNAME_FIELD = 'username'
    # DOCTOR = 1
    # NURSE = 2
    # SURGEN = 3
    #
    # ROLE_CHOICES = (
    #     (DOCTOR, 'Doctor'),
    #     (NURSE, 'Nurse'),
    #     (SURGEN, 'Surgen'),
    # )
    # role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, blank=True, null=True)
    
    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        
    def get_short_name(self):
        return self.username
    
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # if self._password is not None:
        #     password_validation.password_changed(self._password, self)
        #     self._password = None
        