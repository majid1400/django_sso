from django.urls import path
from . import views

urlpatterns = [
    path('sso/login/', views.callback, name='callback'),
    path('sso/registration/', views.registration, name='registration'),
    path('sso/logout/', views.logout, name='logout'),
    path('<str:userName>/', views.home, name='home'),
]
